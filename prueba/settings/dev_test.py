from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DB_NAME', ''),
        'USER': os.environ.get('DB_USER', ''),
        'PASSWORD': os.environ.get('DB_PASSWORD', ''),
        'HOST': os.environ.get('DB_HOST', '127.0.0.1'),
        'PORT': os.environ.get('DB_PORT', '5432')
    }
}

REST_FRAMEWORK = {
    'DATETIME_INPUT_FORMATS': [
        'iso-8601', '%Y-%m-%d %H:%M:%S', '%d/%m/%Y %H:%M:%S', '%d-%m-%Y %H:%M:%S', '%d/%m/%y %H:%M:%S',
        '%d-%m-%y %H:%M:%S', '%Y-%m-%d'
    ]
}

DEBUG = False

ALLOWED_HOSTS = ['localhost', '127.0.0.1']

print('dev_test')
