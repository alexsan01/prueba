from django.urls import path, include

app_name = 'app'

urlpatterns = [
    path('api/', include('app.api.urls', namespace='api')),
]
