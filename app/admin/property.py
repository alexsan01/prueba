from django.contrib import admin

from app.models import PropertyModel


@admin.register(PropertyModel)
class PropertyAdminView(admin.ModelAdmin):
    list_display = ['pk', 'title',  'created_at', 'enable']
