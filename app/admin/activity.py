from django.contrib import admin

from app.models import ActivityModel


@admin.register(ActivityModel)
class ActivityAdminView(admin.ModelAdmin):
    raw_id_fields = ['property']
    list_display = ['pk', 'title', 'created_at', 'enable', 'status']
