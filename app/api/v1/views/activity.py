from django.db.models import Q

from app.api.v1.filter.activity import ActivityFilterBackend
from app.api.v1.serializer.activity import ActivitySerializer
from app.api.v1.views.base_api import BaseModelSetView
from app.models import ActivityModel


class ActivitiView(BaseModelSetView):
    queryset = ActivityModel.objects.filter(~Q(status='cancelled'))
    serializer_class = ActivitySerializer
    permission_classes = []
    filter_backends = [ActivityFilterBackend]

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return super(ActivitiView, self).update(request, *args, **kwargs)

    def perform_destroy(self, instance):
        instance.status = 'cancelled'
        instance.save()
