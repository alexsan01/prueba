from django.utils import timezone
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet


class BaseModelSetView(ModelViewSet):

    def dispatch(self, request, *args, **kwargs):
        r = super(BaseModelSetView, self).dispatch(request, *args, **kwargs)
        out_data = {
            'success': not r.exception,
            'data': r.data,
            'datetime': timezone.now(),
            'message': 'success'
        }

        if r.exception:
            out_data['data'] = None
            out_data['message'] = r.data

        r.data = out_data
        return r

    def rise_api(self, detail, code='error', status_code=400):
        r = APIException(detail=detail, code=code)
        r.status_code = status_code
        return r