from django import forms
from django.db.models import Q
from rest_framework.settings import api_settings

from app.models.activity import ACTIVITY_STATUS


class QueryActivityForms(forms.Form):
    schedule__lte = forms.DateTimeField(required=False, input_formats=api_settings.DATETIME_INPUT_FORMATS)
    schedule__gte = forms.DateTimeField(required=False, input_formats=api_settings.DATETIME_INPUT_FORMATS)
    schedule__gt = forms.DateTimeField(required=False, input_formats=api_settings.DATETIME_INPUT_FORMATS)
    schedule__lt = forms.DateTimeField(required=False, input_formats=api_settings.DATETIME_INPUT_FORMATS)
    status = forms.ChoiceField(choices=ACTIVITY_STATUS, required=False)

    def get_filter(self):
        data = self.cleaned_data
        query = Q()
        for k, v in data.items():
            if v not in [' ', '', None]:
                query &= Q(**{k: v})

        return query
