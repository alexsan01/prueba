from django.db.models import Q
from django.utils import timezone
from rest_framework.filters import BaseFilterBackend

from app.api.v1.forms.query_activity import QueryActivityForms


class ActivityFilterBackend(BaseFilterBackend):
    def get_query_filter(self, request):
        query = QueryActivityForms(data=request.query_params.dict())
        if not query.is_valid():
            schedule = timezone.now()
            data = {
                'schedule__lte': schedule + timezone.timedelta(days=3),
                'schedule__gte': schedule - timezone.timedelta(days=3)
            }
            query = QueryActivityForms(data=data)
            if not query.is_valid():
                return Q()

        return query.get_filter()

    def filter_queryset(self, request, queryset, view):
        if request.method == 'GET' and 'pk' not in request.parser_context.get('kwargs', {}):
            query_filter = self.get_query_filter(request)
            queryset = queryset.filter(query_filter)

        return queryset
