from django.urls import path
from rest_framework import routers

from app.api.v1 import views

app_name = 'app'

router = routers.SimpleRouter()
router.register('activity', views.ActivitiView)

urlpatterns = [

]

urlpatterns += router.urls
