from django.db.models import Q
from django.utils import timezone
from django.utils.translation import ugettext as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from app.api.v1.serializer.property import PropertySerializer
from app.models import ActivityModel


class ActivitySerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(read_only=True)
    condition = serializers.SerializerMethodField(read_only=True)
    survey = serializers.SerializerMethodField(read_only=True)

    def get_condition(self, obj):
        now = timezone.now()
        if obj.status == 'active' and obj.schedule >= now:
            return _('Pendiente a realizar')
        if obj.status == 'active' and obj.schedule < now:
            return _('Atrasada')
        if obj.status == 'done':
            return _('Finalizada')

        return ''

    def get_survey(self, obj):
        return ''

    def to_representation(self, instance):
        data = super(ActivitySerializer, self).to_representation(instance)
        property_data = PropertySerializer(instance=instance.property, many=False).data
        data['property'] = property_data
        return data

    def validate(self, attrs):
        data = super(ActivitySerializer, self).validate(attrs)
        if 'schedule' not in data:
            raise ValidationError(detail=_('Schedule is required'))

        if self.instance is None:
            property = data.get('property')
            if not property.enable:
                raise ValidationError(detail=_('Property Not Active'))
        else:
            if self.instance.status == 'cancelled':
                raise ValidationError(detail=_('Activity Cancelled'))

            property = self.instance.property
            item_list = list(data.keys())
            for i in item_list:
                if i != 'schedule':
                    data.pop(i)

        min_ = data.get('schedule') - timezone.timedelta(hours=1)
        max_ = data.get('schedule') + timezone.timedelta(hours=1)

        if property.activitymodel_set.filter(~Q(status='delete') & Q(schedule__gt=min_, schedule__lt=max_)).exists():
            raise ValidationError(detail=_('Activity Already Created'))

        return data

    class Meta:
        model = ActivityModel
        fields = ['pk', 'schedule', 'title', 'created_at', 'status', 'property', 'condition', 'survey']
