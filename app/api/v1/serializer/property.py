from rest_framework import serializers

from app.models import PropertyModel


class PropertySerializer(serializers.ModelSerializer):
    class Meta:
        model = PropertyModel
        fields = ['pk', 'title', 'address']
