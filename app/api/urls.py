from django.urls import path, include

app_name = 'app'

urlpatterns = [
    path('v1/', include('app.api.v1.urls', namespace='v1'))
]
