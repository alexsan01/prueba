from django.db import models
from django.utils.translation import ugettext as _

from app.models import BaseModel


class PropertyModel(BaseModel):
    title = models.CharField(_('tile'), max_length=255)
    address = models.TextField(_('address'))
    description = models.TextField(_('description'))
    disable_at = models.DateTimeField(null=True, blank=True)
    status = models.CharField(_('status'), max_length=35)
