from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import ugettext as _

from app.models import BaseModel


class SurveyModel(BaseModel):
    activity = models.ForeignKey(verbose_name=_('activity'), to='app.ActivityModel', on_delete=models.CASCADE)
    answers = JSONField(_('answers'), default=dict)
