from django.db import models
from django.utils.translation import ugettext as _

from app.models import BaseModel

ACTIVITY_STATUS = [
    ('created', _('Created')),
    ('active', _('Active')),
    ('pending', _('Pending')),
    ('done', _('Done')),
    ('cancelled', _('Cancelled')),
    ('new_date', _('New Date'))
]


class ActivityModel(BaseModel):
    property = models.ForeignKey(verbose_name=_('property'), to='app.PropertyModel', on_delete=models.CASCADE)
    schedule = models.DateTimeField(_('schedule'))
    title = models.CharField(_('title'), max_length=255)
    status = models.CharField(_('status'), max_length=35, choices=ACTIVITY_STATUS, default='created')
